<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Routing\Controller as BaseController;

class HomeController extends BaseController
{
    public function dashboard()
    {
    	return view("welcome");
    }

    public function matrix(Request $request)
    {
    	$data = array();
    	if(!empty($request)){
    		$data = send_request(2, $request->input('password'));
    	}
    	return view("matrix", compact('data'));

    }


}
