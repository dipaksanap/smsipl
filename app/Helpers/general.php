<?php
  
	function send_request($key, $password){
		// $key == 1 
		//     ? $key = "MD5" 
		//     : ($key == 2 
		//         ? $key = "SHA1" 
		//         : ($key == 3 
		//             ? $key = "SHA256" 
		//             : $key = "SHA512"));
		// $enc = md5($key.$password, false);
		
		try {

			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://www.smsipl.com/Gasco/seat-map.php?key=3ec9864e9db5fb479a338d108503d4612fc65b80",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"auth_key\"\r\n\r\n".$key."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"password\"\r\n\r\n".$password."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
			  CURLOPT_HTTPHEADER => array(
			    "cache-control: no-cache",
			    "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
			    "postman-token: 759ffc35-2298-6bef-8387-cc1cfd8e4d5b"
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);
			if(!isset($err)){
				curl_close($curl);
			}
            return json_decode($response, true);
            
        } catch (\Exception $e) {
            $response = $e->getMessage();
            curl_close($curl);
            return json_decode($response, true);
        }
	    
	}
   
?>