<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SMSIPL</title>

        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <style>
           body{
            font-family: 'Nunito', sans-serif;
            background: #fafafa;
           }
        </style>

        <style>
            body {
                font-family: 'Nunito';
            }
        </style>
    </head>
 
    <body class="antialiased"> 
        <div class="container">
            <div class="navigation mt-3">
                <h2 class="text-center text-primary text-underline">SMSIPL</h2>
            </div>
            <div class="app_body">

                <div class="formbody mx-auto text-center w-50">
                    {{ Form::open(array('url' => '/matrix', 'method' => 'post')) }}
                        <div class="form-group">
                            <input class="form-control" type="password" name="password" required placeholder="Enter Password">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success font-weight-bold">See Matrix</button>
                        </div>
                    {{ Form::close() }}
                </div>
               
            </div>
        </div>


    </body>
</html>
