<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SMSIPL</title>

        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <style>
           body{
            font-family: 'Nunito', sans-serif;
            background: #fafafa;
           }
           td{
           	border: 1px solid;
           }
        </style>

        <style>
            body {
                font-family: 'Nunito';
            }
            table{
				border: 1px solid;
			    margin: auto;
			    border-radius: 8px;
			    padding: 10px;
			}
			td{
				width: 70px;
				height: 70px;
				text-align: center;
				background: #ddd;
			}
			.bg-light{
				border: none;
				background: #fff;
			}
			.bl-0{
				border-left: 1px solid #fafafa;
			}
        </style>
    </head>
 	@php
 	$into = 7;
 	$seat_map = $data['seat_map'];
	$middlespace = array();
	$selected = NULL;
	$one = array("L-1-1-C2", "L-3-1-D2", "L-4-1-E2", "L-5-1-F2", "L-6-1-G2", "L-7-1-H2", "L-8-1-I2", "L-9-1-J2", "L-10-1-K2", "L-11-1-L2");
	$two = array("L-2-3-C3");
	$three = array("L-0-1-B2");
	$letter = array("B","C","D", "E", "F", "G", "H", "J", "K", "L", "M");
 	@endphp

    <body class="antialiased"> 
        <div class="container">
            <div class="navigation mt-3">
                <h2 class="text-center text-primary text-underline">SMSIPL Assignment</h2>
				<blockquote class="blockquote text-center">
				  <p class="mb-0">Seat Matrix.</p>
				  <footer class="blockquote-footer">By Dipak Sanap.</footer>
				</blockquote>            </div>
            <div class="app_body mt-5">
               	 @php
               	 	if(isset($seat_map) || !empty($seat_map)){
						
					echo "<table>";
					foreach ($seat_map as $key => $value) {
						echo "<tr>";
						foreach ($value as $k => $v) {

							for($i = 0; $i<$into; $i++){

								for($j=1; $j<7; $j++){
									foreach ($letter as $l => $ltr) {
										$subrows = explode(',', $v);

										foreach($subrows as $sr => $subrow){
								    		$seat = "L-".$key."-".$i."-".$ltr.$j;

								    		if(0!==$subrow && $seat == $subrow){
								    			$selected = "<p>".$ltr.$j."</p>";
								    			if(in_array($seat, $two)){
								    				echo "<td class='bg-light bl-0'>DOOR</td><td class='bg-light'></td><td class='bg-light'></td>";
									    		}
								    			echo "<td>$selected</td>";
								    			if(in_array($seat, $three)){
								    				echo "<td class='bg-light'></td><td class='bg-light'></td><td class='bg-light'></td>";
									    		}elseif(in_array($seat, $one)){
								    				echo "<td class='bg-light'></td>";
									    		}
								    		}
								    	}
							    		
									}
								}

							}
						}
						echo "</tr>";
					}
					echo "</table>";
				}else{
					echo "Data not received from API";
					return;
				}
               	 @endphp    		
            </div>
        </div>


    </body>
</html>
