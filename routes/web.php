<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as'=>'dashboard', 'uses' => 'App\Http\Controllers\HomeController@dashboard']);
Route::post('/matrix', ['as'=>'see_matrix', 'uses' => 'App\Http\Controllers\HomeController@matrix']);
